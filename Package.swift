// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "AppMind",
    platforms: [
        .macOS(.v10_12), .iOS(.v10), .watchOS(.v4), .tvOS(.v10)
    ],
    products: [        
        .library(name: "AppMind", targets: ["AppMind"])
    ],
    dependencies: [

    ],
    targets: [
        .target(name: "AppMind", dependencies: []),
        .testTarget(name: "AppMindTests", dependencies: ["AppMind"]),
    ]
)
